# %%
import numpy as np
import pandas as pd
import os

data_dir = 'data_all/'
out_dir = 'data/'

for subdir, dirs, files in os.walk(data_dir):
    for f in files:
        df = pd.read_csv(os.path.join( subdir, f))
        df = df.loc[ df['Season'] > 2010 ]
        df.to_csv( os.path.join( out_dir, f + '_red.csv'), index=False)