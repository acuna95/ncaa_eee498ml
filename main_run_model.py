# %%
import numpy as np
import pandas as pd
import argparse
from random import random
from pprint import pprint
from sklearn.metrics import accuracy_score
from itertools import combinations
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
import team_stats_getters as tsg

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--tree_print', action='store_true',help='Print tournament traversal tree')
args = parser.parse_args()

# Good reference
# https://lotanweininger.medium.com/march-madness-machine-learning-2dbacc948874


def test_season_funcs(df, season, team_id):
    tsg.concat_WPoss_LPoss_2_df(df)
    feature_s_funcs = tsg.get_team_stats_hash_funcs(misc_funcs=False)
    for f, func_season in feature_s_funcs.items():
        val = func_season(df, season, team_id)
        print( f'Feature: {f}, val: {val}')


def get_team_stats_df(relevant_past_years=0):

    def get_vld_tids( df, season_w_dict ):
        seasons = season_w_dict.keys()
        red_df = df[['Season', 'WTeamID', 'LTeamID']]
        all_season_range_tids = np.unique( red_df[ ['WTeamID', 'LTeamID'] ].values )

        valid_tids = { tid : 1 for tid in all_season_range_tids }
        for tid in all_season_range_tids:
            for s in seasons:
                s_df = red_df.loc[ red_df['Season']==s ]
                t_s_games_df = s_df.loc[ (s_df['WTeamID']==tid) | (s_df['LTeamID']==tid) ]
                if t_s_games_df.empty and tid in valid_tids:
                    del valid_tids[tid]
        return valid_tids.keys()


    def calc_season_w():
        past_years = relevant_past_years
        relevant_seasons=[current_season-i for i in range( 1 + past_years)]
        year_weights = [1]
        for i in range(past_years):
            year_weights.append( year_weights[-1]/2 ) 
        return dict( zip(relevant_seasons, year_weights) )


    def calc_season_weighted_features(df, valid_tids, season_w_dict):
           # build dict of lists of dicts
        feature_s_funcs = tsg.get_team_stats_hash_funcs(misc_funcs=False)
        # init hash of hashes containing features to 0
        # f_s_res[tid][feature] = val
        f_s_res = { }
        for tid in valid_tids:
            f_s_res[tid] = { f : 0 for f in feature_s_funcs.keys() }
            
        for f, get_f_func in feature_s_funcs.items():
            for tid in valid_tids:
                for s, w in season_w_dict.items():
                    val = get_f_func( df, s, tid)
                    if val is None:
                        print(f'-W- Data {f} missing for team {tid} in season {s}')
                    else:
                        f_s_res[tid][f] += val * w 
        return f_s_res


    def calc_misc_features(valid_tids):
        feature_m_funcs = tsg.get_team_stats_hash_funcs(misc_funcs=True)
        f_s_res = { }
        for tid in valid_tids:
            f_s_res[tid] = { f : 0 for f in feature_m_funcs.keys() }
            
        for tid in valid_tids:
            for f, get_f_func in feature_m_funcs.items():
                f_s_res[tid][f] = get_f_func(tid)
        return f_s_res

    # 
    season_weights_dict = calc_season_w()
    seasons = season_weights_dict.keys()

    # filter seasons
    df = pd.read_csv( data_dir + 'MRegularSeasonDetailedResults.csv')
    df = df.loc[ df['Season'].isin(seasons) ]
    tsg.concat_WPoss_LPoss_2_df(df)

    # filter teams
    team_ids = get_vld_tids( df,  season_weights_dict)
    df = df.loc[ df['WTeamID'].isin(team_ids) ]
    df = df.loc[ df['LTeamID'].isin(team_ids) ]

    # calc team stats
    game_w = calc_season_weighted_features(df, team_ids, season_weights_dict)
    misc = calc_misc_features(team_ids)

    # create data frame with team stats
    features = {}
    for tid in team_ids: 
        features[tid] = { **game_w[tid], **misc[tid]}
    feature_df = pd.DataFrame.from_dict(features, orient='index')

    return feature_df


def get_teamAB_y_df():
    """
    Desc: Reads dataframe of 2020 season and creates a balanced result dataframe
    where TeamA and TeamB win in equal rate. y=1 is TeamA winning.
    """
    df = pd.read_csv( data_dir + 'MRegularSeasonCompactResults.csv')
    df = df.loc[ df['Season'] == current_season ]
    df = df[['WTeamID','LTeamID']]
    unbalanced_df = pd.DataFrame()
    
    # init with A winning everything
    unbalanced_df['TeamA'] = df['WTeamID']
    unbalanced_df['TeamB'] = df['LTeamID']
    unbalanced_df['y']     = 1.0

    # swap randomly
    balanced_df = pd.DataFrame()
    for row in unbalanced_df.itertuples():
        # swap row randomly
        if random() < 0.5:
            new_row = {'TeamA':int(row.TeamB), 'TeamB':int(row.TeamA), 'y':0.0}
        else:
            new_row = {'TeamA':int(row.TeamA), 'TeamB':int(row.TeamB), 'y':1.0}
        balanced_df = balanced_df.append( new_row, ignore_index=True)
        
    return balanced_df


def team_stats_df_row_2_featrs(df, TeamA, TeamB):
    """
    Desc: 
        Uses the team vs team_stats dataframe, locates data for both
        teams, and substracts TeamA stats vs TeamB stats in order to 
        provide a delta_stat between teams for each stat entry
    """
    if (TeamA not in df.index) or (TeamB not in df.index):
        return None

    teamA_stats = df.loc[[TeamA]]
    teamB_stats = df.loc[[TeamB]]

    feature_row = {}
    stats = df.columns
    for stat in stats:
        team_a_val = teamA_stats[stat].values[0]
        team_b_val = teamB_stats[stat].values[0]
        feature_row[stat] = team_a_val - team_b_val
    
    return feature_row


def construct_X_y():
    # get a balance result frame, 0,1s 50/50
    res_df = get_teamAB_y_df()
    Xy_df = pd.DataFrame()
    
    for row in res_df.itertuples():        # create observation
        ftr_dict = team_stats_df_row_2_featrs(team_stats_df, int(row.TeamA), int(row.TeamB))
        if ftr_dict is None:
            continue
        Xy_row = { 'y': row.y }
        Xy_row.update( ftr_dict )
        # append observation to final data frame
        Xy_df = Xy_df.append(Xy_row, ignore_index=True)
    
    y = Xy_df['y'].to_numpy()
    Xy_df.drop(['y'], axis=1, inplace=True)
    X = Xy_df.to_numpy()

    return X, y


def cross_validate(clf, X, y, cv=3):

    X = StandardScaler().fit_transform(X)
    print('Starting training, with crossvalidation of', cv)
    
    y_pred = cross_val_predict( clf, X, y, cv=3)
    scores = cross_val_score( clf, X, y, cv=3)
    clf.fit(X, y)
    y_pred = clf.predict(X)

    mc_train = (y != y_pred).sum()
    print('Total_misclassifications:', mc_train )
    print('Cross validation scores:', scores)


def get_feature_tournament_df():
    """
    Returns all the possible matchups of the tournament
    """
    # team seads data frame
    ts_df = pd.read_csv( data_dir + 'MNCAATourneySeeds.csv')
    ts_df = ts_df.loc[ ts_df['Season'] == current_season ] 

    tourn_teams = list( ts_df['TeamID'] )
    pos_matchups = list( combinations(tourn_teams, 2) )

    X_df = pd.DataFrame()
    for team_a, team_b in pos_matchups:
        ftr_dict = team_stats_df_row_2_featrs(team_stats_df, team_a, team_b)
        if ftr_dict is None:
            continue
        Xy_row = { 'TeamA': team_a, 'TeamB': team_b }
        Xy_row.update( ftr_dict )
        X_df = X_df.append(Xy_row, ignore_index=True)

    matchups = X_df[ ['TeamA', 'TeamB'] ].to_numpy(dtype=int)
    X_df.drop(['TeamA', 'TeamB'], axis=1, inplace=True )
    X_tourn  = X_df.to_numpy()

    return X_tourn, matchups


def get_feature_entry_from_matchup(team_a, team_b):
    # returns A if team A wins
    ftr_dict = team_stats_df_row_2_featrs(team_stats_df, int(team_a), int(team_b))
    if ftr_dict is None:
        return

    X_1_e = np.zeros(len(ts_cols))
    for i, f in enumerate(ts_cols):
        X_1_e[i] = ftr_dict[f]
    return X_1_e.reshape(1,-1)


def construct_tourn_X_y():
    
    res_df = pd.read_csv( data_dir + 'TournamentMatchups.csv')
    X = np.zeros( (len(res_df.index), len( ts_cols)) )
    y = np.zeros( len(res_df.index))

    for i, row in enumerate(res_df.itertuples()):        # create observation
        X_chk = get_feature_entry_from_matchup(int(row.TeamA), int(row.TeamB))
        if X_chk is None: 
            continue
        X[i] = X_chk
        y[i] = row.y

    return X, y



def traverse_tourn( print_pred_and_real_results=False):


    def get_match_winner_tid(TeamA, TeamB):
        X = np.zeros( len( ts_cols) )
        y = np.zeros( 1 )

        X_chk = get_feature_entry_from_matchup( int(TeamA), int(TeamB))
        if X_chk is None:
            return None
        X = X_chk.reshape(1,-1)
        y = clf_lr.predict(X)
        #print( TeamA, TeamB, y )
        return TeamA if y else TeamB


    def region_winner(region_teams):
        ## get winner of each region, and return each reducing stage
        region_reduction_list = [
            region_teams,
            [ 0 for i in range(8) ],
            [ 0 for i in range(4) ],
            [ 0 for i in range(2) ],
            [ 0 ]
        ]
        for s, stage_teams in enumerate(region_reduction_list):

            stage_teams_cnt = len(stage_teams)
            if stage_teams_cnt == 1: # exit loop when 1 team remains
                break

            for i, j in zip( range(stage_teams_cnt//2), range(0,stage_teams_cnt, 2) ):
                region_reduction_list[s+1][i] = get_match_winner_tid( stage_teams[j],stage_teams[j+1] )
                #pprint(region_reduction_list)

        return( region_reduction_list )


    # team conferences
    west    = [1211,1313,1328,1281,1166,1364,1438,1325,1425,1179,1242,1186,1332,1433,1234,1213]
    east    = [1276,1411,1261,1382,1160,1207,1199,1422,1140,1417,1400,1101,1163,1268,1104,1233]
    south   = [1124,1216,1314,1458,1437,1457,1345,1317,1403,1429,1116,1159,1196,1439,1326,1331]
    midwest = [1228,1180,1260,1210,1397,1333,1329,1251,1361,1393,1452,1287,1155,1353,1222,1156]

    ## west, east, south and midwest region winners, and region stages
    w_teams_reduction_list = region_winner(west)
    e_teams_reduction_list = region_winner(east)
    s_teams_reduction_list = region_winner(south)
    mw_teams_reduction_list = region_winner(midwest)
    # final 4 teams
    w_winner = w_teams_reduction_list[-1][0]
    e_winner = e_teams_reduction_list[-1][0]
    s_winner = s_teams_reduction_list[-1][0]
    mw_winner = mw_teams_reduction_list[-1][0]
    # tournament finals
    w_west_east = get_match_winner_tid(w_winner, e_winner)
    w_south_midwest = get_match_winner_tid(s_winner, mw_winner)
    w_tourny = get_match_winner_tid(w_west_east,w_south_midwest)

    ## real results
    w_teams_red_real = [
        [1211,1328,1166,1325,1425,1242,1332,1234],
        [1211,1166,1425,1332],
        [1211,1425],
        [1211],
    ]
    e_teams_red_real = [
        [1276,1261,1160,1199,1417,1101,1268,1104],
        [1276,1199,1417,1104],
        [1276,1417],
        [1417],
    ]
    s_teams_red_real = [
        [1124,1458,1437,1317,1403,1116,1196,1331],
        [1124,1437,1116,1331],
        [1124,1116],
        [1124],
    ]
    mw_teams_red_real = [
        [1228,1260,1333,1329,1393,1452,1353,1222],
        [1260,1333,1393,1222],
        [1333,1222],
        [1222],
    ]
    w_west_east_real = 1211
    w_south_midwest_real = 1124
    w_tourny_real = 1124

    if print_pred_and_real_results:
        pprint(w_teams_reduction_list)
        pprint(w_teams_red_real)

        pprint(e_teams_reduction_list)
        pprint(e_teams_red_real)

        pprint(s_teams_reduction_list)
        pprint(s_teams_red_real)

        pprint(mw_teams_reduction_list)
        pprint(mw_teams_red_real)

    return w_tourny

# %%
# important globals
data_dir = 'data/'
current_season = 2021
team_stats_df = get_team_stats_df(relevant_past_years=2)
ts_cols = team_stats_df.columns

X, y = construct_X_y()

# %%
# Train and test model in normal season games
clf_gb = GradientBoostingClassifier(learning_rate=0.1, verbose=0)
clf_lr = LogisticRegression(max_iter=10000)
cross_validate(clf_gb, X, y, cv=3)
cross_validate(clf_lr, X, y, cv=3)
clf_gb.fit(X,y)
clf_lr.fit(X,y)

print()
y_pred_gb = clf_gb.predict(X)
accuracy = accuracy_score(y, y_pred_gb)
print(f'GradientBossting prediction score: {accuracy:.2f}')

y_pred_lr = clf_lr.predict(X)
accuracy = accuracy_score(y, y_pred_lr)
print(f'LogisticRegression prediction score: {accuracy:.2f}')
print()

# Run model on tournament games
X_tourn, y_tourn = construct_tourn_X_y()

y_pred_tourn_gb = clf_gb.predict(X_tourn)
accuracy = accuracy_score(y_tourn, y_pred_tourn_gb)
print(f'Tournament games GB model accuracy: {accuracy:.2f}')

y_pred_tourn_lr = clf_lr.predict(X_tourn)
accuracy = accuracy_score(y_tourn, y_pred_tourn_lr)
print(f'Tournament games LR model accuracy: {accuracy:.2f}')

print()
w_tourny = traverse_tourn(print_pred_and_real_results=args.tree_print)
print('Tournament winner', w_tourny)
# %%
