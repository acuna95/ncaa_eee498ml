# NCAA Tournament Prediction
This project contains all the source code and data necessary to train a Machine Learning model capable of predicting NCAA Men's Basketball games. Project objective consisted on predicting 2021 March Madness Tournament results. 

File structure: 
* `data/` - contains all *.csv files needed to train and test the model
* `team_stats_getters.py` - Functions which parse and get team box score stats as well as miscelaneus stats
* `main_run_model.py` - Main script. 

## Main flow
1. Constructs data frame with stats of every team **team_stats_df**
2. Constructs features numpy vector based on team stats deltas between teams of a matchup and its corresponding target vector **X, y**
3. Trains model with season 2021 games
4. Applies model to both the 2021 season as well as the tournament
5. Traverses the tournament with the model predicting every game, were only the initial stage of the tournament is given

## Replicating results

```shell
python main_run_model.py
```

It might be useful to take a look at the global variables **team_stats_df**, **X**, **y** to understand feature vector better
```shell
python -i main_run_model.py
```

If the traversing tree is of interest, argument in place in order to enable printing
```shell
python main_run_model.py -p
```

## Dependencies 
* numpy
* pandas
* sklearn