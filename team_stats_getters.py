from collections import Counter
import pandas as pd
import numpy as np

data_dir = 'data/'

### GAME STAT feature getters ------------------------------------
def concat_WPoss_LPoss_2_df(df):
    # add possesion count for both teams to df, formula according to: 
    # https://www.basketball-reference.com/about/glossary.html#fta
    W_fga = df['WFGA'] + df['WFGA3']
    W_fgm = df['WFGM'] + df['WFGM3']
    W_stats = W_fga + 0.4*df['WFTA'] \
        - 1.07*( df['WOR']/(df['WOR']+df['LDR']) )*(W_fga - W_fgm)  \
        + df['WTO']

    L_fga = df['LFGA'] + df['LFGA3']
    L_fgm = df['LFGM'] + df['LFGM3']
    L_stats =  L_fga + 0.4*df['LFTA'] \
        - 1.07*( df['LOR']/(df['LOR']+df['WDR']) )*(L_fga - L_fgm) \
        + df['LTO']

    game_poss = W_stats + L_stats
    df['WPoss'] = game_poss/2
    df['LPoss'] = game_poss/2


def get_poss_Rtgs(df, season, team_id, out_sel):

    W_df = df.loc[ (df['WTeamID']==team_id) & (df['Season']==season) ]
    L_df = df.loc[ (df['LTeamID']==team_id) & (df['Season']==season) ]
    if W_df.empty or L_df.empty:
        return None

    total_poss = W_df['WPoss'].sum() + L_df['LPoss'].sum()

    # calc team possesions per game
    if out_sel == 0:
        total_points = W_df['WScore'].sum() + L_df['LScore'].sum()
        pts_per_poss = total_points / total_poss
        ORtg = pts_per_poss * 100
        res = ORtg
    elif out_sel == 1:
        # calc defensive rating
        opp_total_points = L_df['WScore'].sum() + W_df['LScore'].sum()
        opp_total_poss   = L_df['WPoss'].sum() + W_df['LPoss'].sum()
        opp_pts_per_poss = opp_total_points/opp_total_poss
        DRtg = opp_pts_per_poss * 100
        res = DRtg
    elif out_sel == 2: 
        # calc possesions per game
        total_games = len( W_df.index ) + len(L_df.index)
        poss_per_game = total_poss/total_games
        res = poss_per_game

    return res


def get_poss_pg_avg(df, season, team_id):
    return get_poss_Rtgs(df, season, team_id, 0)


def get_ORtg(df, season, team_id):
    return get_poss_Rtgs(df, season, team_id, 1)


def get_DRtg(df, season, team_id):
    return get_poss_Rtgs(df, season, team_id, 2)


def get_assists(df, season, team_id ): 

    W_df = df.loc[ (df['WTeamID']==team_id) & (df['Season']==season) ]
    L_df = df.loc[ (df['LTeamID']==team_id) & (df['Season']==season) ]
    if W_df.empty or L_df.empty:
        return None

    total_assists = W_df['WAst'].sum() + L_df['LAst'].sum()
    total_games = len( W_df.index ) + len(L_df.index)
    return total_assists / total_games


def get_3pt_perc(df, season, team_id): 

    W_df = df.loc[ (df['WTeamID']==team_id) & (df['Season']==season) ]
    L_df = df.loc[ (df['LTeamID']==team_id) & (df['Season']==season) ]
    if W_df.empty or L_df.empty:
        return None

    total_3pta = W_df['WFGA3'].sum() + L_df['LFGA3'].sum()
    total_3ptm = W_df['WFGM3'].sum() + L_df['LFGM3'].sum()
    return total_3ptm/total_3pta * 100


def get_fg_perc(df, season, team_id): 

    W_df = df.loc[ (df['WTeamID']==team_id) & (df['Season']==season) ]
    L_df = df.loc[ (df['LTeamID']==team_id) & (df['Season']==season) ]
    if W_df.empty or L_df.empty:
        return None

    total_fga = W_df['WFGA'].sum() + L_df['LFGA'].sum()
    total_fgm = W_df['WFGM'].sum() + L_df['LFGM'].sum()
    return total_fgm/total_fga * 100


def get_W_perc(df, season, team_id): 

    W_df = df.loc[ (df['WTeamID']==team_id) & (df['Season']==season) ]
    L_df = df.loc[ (df['LTeamID']==team_id) & (df['Season']==season) ]
    if W_df.empty or L_df.empty:
        return None

    total_games = len( W_df.index ) + len( L_df.index )
    return len( W_df.index ) / total_games * 100


### TEAM MISC info ---------------------------------------------

def get_coach_yrs(team_id):
    ## determine how many years the current coach has coached a team
    TeamCoaches = pd.read_csv(data_dir + 'MTeamCoaches.csv')
    TeamCoaches = np.array(TeamCoaches)
    index_ID_info = np.array(np.where(TeamCoaches[:,1] == team_id))
    length_info = len(np.transpose(index_ID_info))
    ID_info = TeamCoaches[index_ID_info,:]
    ID_info_2020 = np.array(ID_info[:,length_info - 1])
    if (ID_info_2020[0,0] != 2021):
        return(0)
    else:
        current_coach = ID_info_2020[0,4]
        num_yrs = []
        for i in range(length_info - 1):
            info_past_yr = np.array(ID_info[:,(length_info - 1) - i])
            coach_past_yr = info_past_yr[0,4]
            if ( current_coach == coach_past_yr):
                year = 1
                num_yrs.append(year)
                i+=1
            else:
                return(sum(num_yrs))
        sum_num_yrs = sum(num_yrs)+1
    return(sum_num_yrs)


def get_tourn_experience( id ):
    year = 2021
    Tourney_results = pd.read_csv( data_dir + 'MNCAATourneyCompactResults.csv')
    Tourney_results = Tourney_results[Tourney_results["Season"] > 2013]
    Tourney_results =Tourney_results[Tourney_results["Season"] <= year]
    Team_wins = Tourney_results.iloc[:,2]
    Team_Losses = Tourney_results.iloc[:,4]
    duplicate_dict_W = Counter(Team_wins)
    duplicate_dict_L = Counter(Team_Losses)
    if id in (Tourney_results.values) :
        #exp = {'TeamID': id, 'Wins':duplicate_dict_W[id],'losses': duplicate_dict_L[id],'# of tourney games':duplicate_dict_W[id] +duplicate_dict_L[id],
    #'Win %':round((duplicate_dict_W[id] /(duplicate_dict_W[id] + duplicate_dict_L[id]))*100,2)}
        #W = duplicate_dict_W[id]
        #L = duplicate_dict_L[id]
        #Win_per =round((duplicate_dict_W[id] /(duplicate_dict_W[id] + duplicate_dict_L[id]))*100,2)
        num_games = duplicate_dict_W[id] +duplicate_dict_L[id]
    else:
        #exp = Teams.loc[Teams.eq(id).any(1)]
       # exp = exp.to_dict()
        #exp = {'TeamID': id, 'Wins':0,'losses': 0,'# of tourney games':0 ,'Win %':0}
        #W = 0; L =0
        #Win_per = 0
        num_games = 0
    return( num_games )


def get_conference(id):
    MConferenceTourneyGames = pd.read_csv( data_dir + 'MConferenceTourneyGames.csv')
    Conferences_in_tourney = MConferenceTourneyGames[MConferenceTourneyGames["Season"] >2013]

    print(Conferences_in_tourney.values)
    if id in (Conferences_in_tourney.values) :
        team_conf = Conferences_in_tourney[Conferences_in_tourney.eq(id).any(1)]
        y = team_conf.iloc[0,1]
    else:
        y = 0
    return(y)


## hash of functions ------------------------
def get_team_stats_hash_funcs(misc_funcs=False):
    team_stats_funcs = {}
    if misc_funcs:
        team_stats_funcs.update({
            'coach_yexp' : get_coach_yrs,
            'tournm_exp' : get_tourn_experience,
            #'conf'       : Conference
        })
    else: 
        team_stats_funcs.update({
            'avgposs_pg' : get_poss_pg_avg,
            'ORtg'       : get_ORtg,
            'DRtg'       : get_DRtg,
            'avg3pt_perc': get_3pt_perc,
            'avgfg_perc' : get_fg_perc,
            'avg_assists': get_assists,
            'team_w_perc': get_W_perc,
        })
    return team_stats_funcs
    